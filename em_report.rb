require 'date'
require 'json'
require 'net/http'
require 'open3'

ACTIVITY_PERIOD = 7
PER_PAGE = 100
PAGE_LIMIT = 5
IGNORED_PROJECT_ID = []
GROUPS = %w[gitlab-com gitlab-org]
PROJECT_ID_TO_URL = {
  278964 => 'gitlab-org/gitlab',
  7764 => 'gitlab-com/www-gitlab-com'
}
TARGET_TYPES_TO_REFERENCE = {
  'MergeRequest' => '!',
  'Issue' => '#',
  'Epic' => '&'
}
TARGET_TYPES_TO_ROUTE = {
  'MergeRequest' => 'merge_requests',
  'Issue' => 'issues',
  'Epic' => 'epics'
}
FILE_NAME = 'body.md'
DEFAULT_ISSUE_IID = 1
DATE_FORMAT = ->(date) { date.strftime('%F') }
EVENTS_API_URL = ->(user_id, date_from, date_to) { "users/#{user_id}/events?after=#{DATE_FORMAT.call(date_from)}&before=#{DATE_FORMAT.call(date_to)}" }
NEW_VERSION_WARNING = 'new version of glab has been released'

GlabError = Class.new(StandardError)

def format_date(date)
  date.strftime('%F')
end

def merge_request_graphql_query(group_path, date_from, date_to, username)
  <<~GRAPHQL
    query {
      group(fullPath: "#{group_path}") {
        mergeRequests(createdAfter: "#{DATE_FORMAT.call(date_from)}", createdBefore: "#{DATE_FORMAT.call(date_to)}", authorUsername: "#{username}", first: #{PER_PAGE}) {
          nodes {
            webUrl
            createdAt
            project {
              fullPath
            }
          }
        }
      }
    }
  GRAPHQL
end

def call_glab_api(command)
  stdout, stderr, status = Open3.capture3("glab api #{command}")
  return stdout if status.success? && (stderr.strip.empty? || stderr.include?(NEW_VERSION_WARNING))

  raise GlabError, stderr
end

def respond_with_error(method_name, error)
  puts "Error in ##{method_name} [#{error.class}: #{error.message.strip}]"
  puts "...failed."
  exit 1
end

def collect_and_group_events(user_id, date_from, date_to)
  events = fetch_events(user_id, date_from, date_to)
  raw_events = events

  events
    .uniq { |event| event['id'] }
    .sort_by { |event| event['created_at'] }
    .group_by { |event| event['action_name'] }
    .then { |events| [events, raw_events] }
end

def collect_merge_requests(username, date_from, date_to)
  GROUPS
    .flat_map { |group_path| fetch_graphql_api(merge_request_graphql_query(group_path, date_from, date_to, username)).dig('data', 'group', 'mergeRequests', 'nodes') }
    .sort_by { |merge_request| merge_request['createdAt'] }
    .group_by { |merge_request| merge_request.dig('project', 'fullPath') }
    .map do |group_path, merge_requests|
      merge_requests_markdown = merge_requests.map { |mr| "- [ ] #{mr['webUrl']}+ (`#{mr['createdAt']}`)" }.join("\n")
      <<~MARKDOWN
        ### `#{group_path}`: #{merge_requests.size}

        #{merge_requests_markdown}
      MARKDOWN
    end.join("\n")
end

def fetch_graphql_api(graqhql_query)
  raw = call_glab_api("graphql -f query='#{graqhql_query}'")
  JSON.parse(raw)
rescue => e
  respond_with_error(:fetch_graphql_api, e)
end

def fetch_events(user_id, date_from, date_to)
  raw = call_glab_api("--paginate #{EVENTS_API_URL.call(user_id, date_from, date_to)}")
  JSON.parse(raw.gsub("][", ","))
rescue => e
  respond_with_error(:fetch_events, e)
end

def prepare_url(title, project_id, type, iid, note_id = nil)
  if PROJECT_ID_TO_URL.key?(project_id)
    target_type = TARGET_TYPES_TO_REFERENCE[type]

    if note_id.nil?
      "#{PROJECT_ID_TO_URL[project_id]}#{target_type}#{iid}+"
    else
      "https://gitlab.com/#{PROJECT_ID_TO_URL[project_id]}/-/#{TARGET_TYPES_TO_ROUTE[type]}/#{iid}#note_#{note_id}"
    end
  else
    target_type = TARGET_TYPES_TO_ROUTE[type]

    "[#{title}](https://gitlab.com/api/v4/projects/#{project_id}/#{target_type}/#{iid})"
  end
end

def prepare_commit_url(project_id, sha)
  "https://gitlab.com/api/v4/projects/#{project_id}/repository/commits/#{sha}"
end

def merge_request_events_with_action(events, action)
  events
    .fetch(action, [])
    .select { |event| event['target_type'] == 'MergeRequest' }
    .map { |event| "- [ ] #{prepare_url(event['target_title'], event['project_id'], 'MergeRequest', event['target_iid'])} (`#{event['created_at']}`)" }
    .join("\n")
end

def issue_events_with_action(events, action)
  events
    .fetch(action, [])
    .select { |event| event['target_type'] == 'Issue' || event['target_type'] == 'Epic' || event['target_type'] == 'WorkItem' }
    .map { |event| "- [ ] #{prepare_url(event['target_title'], event['project_id'], 'Issue', event['target_iid'])} (`#{event['created_at']}`)" }
    .join("\n")
end

EVENTS_BY_TARGET_TYPE = {
  'MergeRequest' => %w[opened approved accepted closed],
  'Issue' => %w[opened closed],
}

def prepare_merge_requests(events)
  EVENTS_BY_TARGET_TYPE['MergeRequest'].map do |action|
    action_events = merge_request_events_with_action(events, action)
    next if action_events.empty?

    <<~MARKDOWN
      #### #{action.capitalize} Merge Requests

      #{action_events}
    MARKDOWN
  end.compact.join("\n")
end

def prepare_issues(events)
  EVENTS_BY_TARGET_TYPE['Issue'].map do |action|
    action_events = issue_events_with_action(events, action)
    next if action_events.empty?

    <<~MARKDOWN
      ### #{action.capitalize} Issues

      #{action_events}
    MARKDOWN
  end.compact.join("\n")
end

def prepare_comments(events)
  events
    .fetch('commented on', [])
    .reject { |event| IGNORED_PROJECT_ID.include?(event['project_id']) }
    .group_by { |event| event['target_title'] }
    .map do |title, events|
      first_event = events.first
      formatted_events = events.map do |event|
        [
          "   - [ ] [`#{event['created_at']}`](#{prepare_url(title, first_event['project_id'], first_event.dig('note', 'noteable_type'), first_event.dig('note', 'noteable_iid'), event['target_iid'])}):",
          event.dig('note', 'body').split("\n").map { |line| "      > #{line}" }.join("\n")
        ].join("\n")
      end.join("\n\n")

      <<~MARKDOWN
        * commented in #{prepare_url(title, first_event['project_id'], first_event.dig('note', 'noteable_type'), first_event.dig('note', 'noteable_iid'))}:
        #{formatted_events}
      MARKDOWN
    end.join("\n")
end

def prepare_pushes(events)
  events
    .values_at('pushed new', 'deleted', 'pushed to')
    .flatten
    .compact
    .sort_by { |event| event['created_at'] }
    .map do |event|
      commit_details = event.dig('push_data', 'commit_title').nil? ? nil : "[#{event.dig('push_data', 'commit_title')}](#{prepare_commit_url(event['project_id'], event.dig('push_data', 'commit_to'))}) "
      "- [ ] **#{event['action_name']}** `#{event.dig('push_data', 'ref_type')}` `#{event.dig('push_data', 'ref')}` #{commit_details}`#{event['created_at']}`"
    end.join("\n")
end

def prepare_note(username, user_id, date_to)
  date_30_days_ago = date_to.prev_day(30)
  date_from = date_to.prev_day(ACTIVITY_PERIOD)
  events, raw_events = collect_and_group_events(user_id, date_from, date_to)
  mrs_from_last_week = collect_merge_requests(username, date_from, date_to)
  mrs_from_last_month = collect_merge_requests(username, date_30_days_ago, date_to)

  <<~MARKDOWN
  <details>
  <summary>
  <strong>Activity for #{DATE_FORMAT.call(date_from)} - #{DATE_FORMAT.call(date_to)}</strong>
  </summary>

  ## Merge Requests

  ### MRs created within last 7 days

  #{mrs_from_last_week}

  ### MRs created within last 30 days

  #{mrs_from_last_month}

  ### Merge Request Activities

  #{prepare_merge_requests(events)}

  ## Pushes

  #{prepare_pushes(events)}

  ## Issues

  #{prepare_issues(events)}

  ## Comments

  #{prepare_comments(events)}

  <details>
  <summary>
  <i>Generated by [🧰 Activity Scripts](https://gitlab.com/alan/activity-scripts/) (<strong>v1.1.2</strong>)</i>
  </summary>

  ##### Used APIs

  ```
  * https://gitlab.com/api/v4/#{EVENTS_API_URL.call(user_id, date_from, date_to)}
  ```

  ##### GraphQL Queries

  * fetch MRs from last 7 days
  ```graphql
  #{merge_request_graphql_query("gitlab-org", date_from, date_to, username)}
  ```
  * fetch MRs from last 30 days
  ```graphql
  #{merge_request_graphql_query("gitlab-org", date_30_days_ago, date_to, username)}
  ```
  </details>
  </details>
  MARKDOWN
end

def create_note(project_id, username, user_id, date_to, issue_iid)
  note = prepare_note(username, user_id, date_to)
  File.open(FILE_NAME, 'w+') { |f| f.write(note) }
  call_glab_api("projects/#{project_id}/issues/#{issue_iid}/notes -X POST -F body=@#{FILE_NAME}")
rescue => e
  respond_with_error(:create_note, e)
end

puts "Creating note: create_note(#{ENV['CI_PROJECT_ID']}, #{ENV['ACTIVITY_USERNAME']}, #{ENV['ACTIVITY_USER_ID']}, #{DATE_FORMAT.call(Date.today.prev_day)}, #{ENV['ACTIVITY_ISSUE_IID'] || DEFAULT_ISSUE_IID})..."
create_note(ENV['CI_PROJECT_ID'], ENV['ACTIVITY_USERNAME'], ENV['ACTIVITY_USER_ID'], Date.today.prev_day, ENV['ACTIVITY_ISSUE_IID'] || DEFAULT_ISSUE_IID)
puts "...done."
