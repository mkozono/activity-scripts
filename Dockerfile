FROM ruby:3.2-alpine

RUN apk add --no-cache glab

RUN mkdir /gitlab
WORKDIR /gitlab

COPY em_report.rb ./

SHELL ["/bin/bash", "-c"]
CMD ["ruby", "em_report.rb"]
